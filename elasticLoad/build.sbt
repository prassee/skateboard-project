name := """elasticLoad"""

version := "0.1"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-contrib" % "2.3.9",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.9",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "commons-io" % "commons-io" % "2.4" % "test",
  "rome" % "rome" % "1.0",
  "com.typesafe.slick" %% "slick" % "3.1.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.h2database" % "h2" % "1.3.176",
  "com.zaxxer" % "HikariCP" % "2.4.1" ,
  "com.sksamuel.elastic4s" %% "elastic4s-core" % "1.6.2",
  "org.scalaj" %% "scalaj-http" % "1.1.4",
  "org.rometools" % "rome-fetcher" % "1.2")
