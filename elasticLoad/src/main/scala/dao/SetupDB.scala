package dao

import models.{Feed, Feeds}

import scala.io.Source

/**
  *
  */
object SetupDB extends App with DBAccessTrait {

  import dbConfig.driver.api._

  lazy val feedSource = Source.fromFile(this.getClass.getResource("/feeds.csv").getPath).getLines()

  val allFeeds: Seq[Feed] = (for {
    feed <- feedSource
  } yield Feed(None, feed.split(",")(0), feed.split(",")(1), 0, 0)).toSeq

  lazy val feeds = TableQuery[Feeds]

  val feedSchema = (feeds.schema)

  db.run {
    DBIO.seq(
      feedSchema.create,
      feeds ++= allFeeds
    )
  }

  db.close
}

