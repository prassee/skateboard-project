package dao

import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile
import slick.jdbc.JdbcBackend

/**
  *
  */
trait DBAccessTrait {
  val dbConfig = DatabaseConfig.forConfig[JdbcProfile]("h2")
  val db: JdbcBackend#DatabaseDef = dbConfig.db
}
