package dao

import models.{Feed, Feeds}

import scala.concurrent.Future


object FeedDao extends DBAccessTrait {

  import dbConfig.driver.api._

  val feeds = TableQuery[Feeds]

  def getFeedDetails(id: Long): Future[Seq[Feed]] = {
    try {
      val q = feeds.filter(x => x.id === id)
      val res = q.result
      db.run(res)
    } catch {
      case e: SlickException => throw new SlickException(e.getMessage)
    } finally {
      // db.close()
    }
  }

  def getLatestEpoch(id: Long): Future[Seq[Long]] = {
    try {
      val q = feeds.filter(x => x.id === id).map { x => x.lastRead }
      val res = q.result
      db.run(res)
    } catch {
      case e: SlickException => throw new SlickException(e.getMessage)
    } finally {
      // db.close()
    }
  }

  def updateEpoch(id: Long, epoch: Long): Future[Int] = {
    try {
      val q = feeds.filter(x => x.id === id).map { x => x.lastRead }
      val res = q.update(epoch)
      db.run(res)
    } catch {
      case e: SlickException => throw new SlickException(e.getMessage)
    } finally {
      // db.close()
    }
  }

  def startID(): Future[Seq[Feed]] = {
    try {
      val q = feeds.map { x => x }.take(1)
      val res = q.result
      db.run(res)
    } catch {
      case e: SlickException => throw new SlickException(e.getMessage)
    } finally {
      // db.close()
    }
  }


  def getAllFeeds: Future[Seq[Feed]] = {
    try {
      val q = feeds.map { x => x }
      val res = q.result
      db.run(res)
    } catch {
      case e: SlickException => throw new SlickException(e.getMessage)
    } finally {
      // db.close()
    }
  }


}
