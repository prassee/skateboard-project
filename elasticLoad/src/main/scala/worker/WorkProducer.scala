package worker

import akka.actor.{Actor, ActorLogging, ActorRef}
import dao.FeedDao

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.forkjoin.ThreadLocalRandom

object WorkProducer {
  case object Tick
}

class WorkProducer(frontend: ActorRef) extends Actor with ActorLogging {
  import WorkProducer._
  import context.dispatcher
  def scheduler = context.system.scheduler
  def rnd = ThreadLocalRandom.current
  var n = 0

  val feeds = Await.result(FeedDao.getAllFeeds, Duration.Inf)
  //val startId = Await.result(SlickMain.startID(), Duration.Inf)(0).id.get
  //var currCount = startId

  //println(feedCount)

  def nextWorkId(): String = { n = n + 1; n.toString } //UUID.randomUUID().toString

  override def preStart(): Unit =
    //scheduler.scheduleOnce(2.seconds, self, Tick)
    self ! Tick

  def receive = {
    case Tick =>
      println("Scheduled tick")
      
      feeds.foreach{feed =>
        val work = Work(nextWorkId(), feed)
        frontend ! work
        
      }
      
      context.become(waitAccepted, discardOld = false)
      
      /*
      while (currCount < (startId + feedCount)) {
        val feed = Await.result(SlickMain.getFeedDetails(currCount), Duration.Inf)
        currCount = currCount + 1
        println(feed(0).id)
        val work = Work(nextWorkId(), feed(0))

        println("Sending work")
        frontend ! work
        context.become(waitAccepted(work), discardOld = false)
      }
      * 
      */
      
  }

  def waitAccepted: Actor.Receive = {
    case Frontend.Ok =>
      context.unbecome()
      scheduler.scheduleOnce(120.seconds, self, Tick)
      //Thread.sleep(60000)
      //self ! Tick
    /*
    case Frontend.NotOk =>
      log.info("Work not accepted, retry after a while")
      scheduler.scheduleOnce(3.seconds, frontend, work)
      * 
      */
  }

}