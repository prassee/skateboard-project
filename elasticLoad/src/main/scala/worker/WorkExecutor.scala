package worker

import java.net.URL
import java.text.SimpleDateFormat

import akka.actor.Actor
import com.sun.syndication.feed.synd.SyndEntry
import dao.FeedDao._
import models.Feed
import org.rometools.fetcher.impl._

import scala.collection.JavaConversions._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scalaj.http.Http

class WorkExecutor extends Actor {

  val feedFetcher = new HttpURLFeedFetcher()

  case class RSSEntry(urlLink: String, date: String, title: String, desc: String)

  def sendPostData(str: String) = {
    Http("http://localhost:9000/req/data").postData(str).header("content-type", "application/json").asString.code
  }

  def getEpoch(dateString: String): Long = {
    val df = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy")
    val date = df.parse(dateString)
    val epoch = date.getTime
    epoch
  }

  def findNewLinks(f: Feed) = {
    val feed = feedFetcher.retrieveFeed(new URL(f.link))

    val dbLatestEpoch = Await.result(getLatestEpoch(f.id.getOrElse(0L)), Duration.Inf)(0)

    var latestEpoch = dbLatestEpoch

    feed.getEntries.toList.reverse.foreach { x =>
      val syndEntry = x.asInstanceOf[SyndEntry]
      val entryEpoch = getEpoch(syndEntry.getPublishedDate.toString)

      val urlLink = syndEntry.getLink
      val date = syndEntry.getPublishedDate.toString
      val desc = syndEntry.getDescription.getValue.replaceAll("<[^>]*>", "").replaceAll("\"", "")
      val title = syndEntry.getTitle

      val rss = RSSEntry(urlLink, date, title, desc)
      println(rss)

      if (entryEpoch > latestEpoch) {
        println(entryEpoch + " " + syndEntry.getPublishedDate + "  " + syndEntry.getTitle)

        sendPostData(
          s"""
                       {
                          "source" : "${f.source}"  ,
                          "title"  : "$title",
                          "date"   : ${getEpoch(date).toString},
                          "content" : "$title",
                          "image"   : ""
                       }
                       """)

        latestEpoch = entryEpoch
        updateEpoch(f.id.get, latestEpoch)
      }
    }

  }

  def receive = {
    case f: Feed =>
      try {
        findNewLinks(f)
        sender() ! Worker.WorkComplete("Done")
      } catch {
        case ex: Exception =>
          println(ex.getCause)
          println("Couldn't parse the link " + ex.getMessage)
          sender() ! Worker.WorkComplete("0")
      }
  }

}