package models

import slick.driver.H2Driver.api._

/*
value code for currState
 0 -> UNREACHABLE
 1 -> READ
 2 -> UPDATING
 3 -> UNCHANGED
 4 -> DONE
 */
case class Feed(id: Option[Long], source: String, link: String, lastEpoch: Long, currState: Int)

class Feeds(tag: Tag) extends Table[Feed](tag, "FEEDS") {
  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def source = column[String]("SOURCE")

  def link = column[String]("LINK")

  def lastRead = column[Long]("LAST_READ_EPOCH")

  def currState = column[Int]("CURR_STATE")

  def * = (id.?, source, link, lastRead, currState) <>(Feed.tupled, Feed.unapply)
}


