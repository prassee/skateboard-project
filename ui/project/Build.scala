import sbt._
import sbt.Keys._
import org.scalajs.sbtplugin._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

object Build extends sbt.Build {
  val buildOrganisation = "ui"
  val buildVersion = "skateboard-0.1"
  val buildScalaVersion = "2.11.6"
  val buildScalaOptions = Seq(
    "-unchecked", "-deprecation"
    , "-encoding", "utf8"
    , "-Xelide-below", annotation.elidable.ALL.toString
  )

  lazy val main = Project(id = "ui", base = file("."))
    .enablePlugins(ScalaJSPlugin)
    .settings(
      libraryDependencies ++= Seq(
        "io.github.widok" %%% "widok" % "0.2.2"
      )
      , organization := buildOrganisation
      , version := buildVersion
      , scalaVersion := buildScalaVersion
      , scalacOptions := buildScalaOptions
      , persistLauncher := true
    )
}