package ui

import org.scalajs.dom._
import org.widok._
import org.widok.bindings.HTML._

object Main extends PageApplication {

  val response = Var("waiting for News .... ")
  val ws = new WebSocket("ws://localhost:9898/")
  ws.onmessage = (x: MessageEvent) => response.:=(x.data.toString)

  def view() = Inline(
    Heading.Level1("Welcome to SkateBoard!"),
    Paragraph(response)
  )

  def ready() {
    log("Page loaded.")
  }
}
