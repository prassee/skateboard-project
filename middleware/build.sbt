name := "middleware"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq("com.tumblr" %% "colossus" % "0.6.6",
  "org.java-websocket" % "Java-WebSocket" % "1.3.0")
    