package middleware

import java.net.InetSocketAddress

import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer

import scala.collection.mutable.ListBuffer

class WSTransportSocket(port: Int) extends WebSocketServer(new InetSocketAddress(port)) {

  private val map = ListBuffer[WebSocket]()

  def onOpen(conn: WebSocket, chs: ClientHandshake): Unit = {
    println("opening connection")
    map += conn
  }

  def onClose(conn: WebSocket, code: Int, reason: String, remote: Boolean): Unit = {
    println("removing connection")
    map -= conn
  }

  def onError(conn: WebSocket, ex: Exception): Unit = {}

  def onMessage(conn: WebSocket, message: String): Unit = {
    println(message)
  }

 def sendToAll(d: String) = {
   
 }
  
  def broadCastMessages(msg: String) = {
    map.foreach(x => x.send(msg))
    // map.foldLeft("")((a, b) => a + b)
     
  }

}
