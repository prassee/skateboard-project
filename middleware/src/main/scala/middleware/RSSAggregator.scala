package middleware

import colossus._
import service._
import protocols.http._
import UrlParsing._
import HttpMethod._
import akka.util.ByteString

/**
  *
  */
object RSSAggregator extends App {

  implicit val ioSystem = IOSystem()

  private val webSocketTransport = new WSTransportSocket(9898)

  webSocketTransport.start()

  Service.become[Http]("rss-agg", 9000) {
    case request@Post on Root / "req" / "data" =>
      //webSocketTransport.broadCastMessages("yoyo")
      
      webSocketTransport.broadCastMessages(request.entity.get.utf8String)
      Callback.successful(request.ok("sent"))
  }

}